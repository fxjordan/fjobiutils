package de.fjobilabs.util;

import java.util.Calendar;

import java.io.*;
import java.nio.charset.*;

/**
 * Helper class to easily print messages to the default output stream.
 * 
 * @author Felix Jordan
 * @since 22.01.2015 - 22:50:26
 * @version 1.0
 */
public class Log {
	
	private static final int MAX_TAG_LENGTH = 20;
	private static final String SEPERATOR = "    ";
	private static final String LOG_TAG = "Log";
	
	private static PrintStream sSystemOut;
	private static PrintStream sSystemErr;
	private static StringBuilder sLogFileBuffer;
	private static PrintStream sLogFileOut;
	private static File sLogFile;
	private static File sLogFolder;
	private static Level sLevel;
	
	static {
		sSystemOut = System.out;
		sSystemErr = System.err;
		System.setOut(new PrintStream(new AutoLogOutputStream(Level.INFO, "System.out")));
		System.setErr(new PrintStream(new AutoLogOutputStream(Level.ERROR, "System.err")));
		sLogFileBuffer = new StringBuilder();
		
		Log.i(LOG_TAG, "Initializing log system...");
		Log.i(LOG_TAG, "System.out and System.err successfull mapped to an internal log stream!");
		Log.i(LOG_TAG, "Loading properties file...");
		Properties.load();
		Log.i(LOG_TAG, "Properties initialized!");
		if (Properties.logFolder == null) {
			Log.w(LOG_TAG, "No log folder set! Storing messages in internal buffer until it is set at runtime.");
		} else {
			sLogFolder = new File(Properties.logFolder);
			if (sLogFolder.exists()) {
				if (sLogFolder.isDirectory()) {
					sLogFileOut = initLogFile();
					writeBufferToLogFile();
				} else {
					Log.w(LOG_TAG, "Log folder '" + sLogFolder.getAbsolutePath() + "' is no directory!");
				}
			} else {
				Log.w(LOG_TAG, "Log folder '" + sLogFolder.getAbsolutePath() + "' does not exist!");
			}
		}
		
		if (Properties.logLevel != null) {
			for (Level level: Level.values()) {
				if (level.toString().equalsIgnoreCase(Properties.logLevel)) {
					sLevel = level;
				} else if (level.mShort.equalsIgnoreCase(Properties.logLevel)) {
					sLevel = level;
				}
			}
			if (sLevel == null) {
				Log.w(LOG_TAG, "Log level '" + Properties.logLevel + "' does not exist! Using defaults.");
				sLevel = Level.DEBUG;
			} else {
				Log.i(LOG_TAG, "Using log level '" + sLevel + "'");
			}
		} else {
			Log.w(LOG_TAG, "No log level defined in properties! Using defaults!");
		}
		
		Log.i(LOG_TAG, "Log system initialized!");
	}
	
	private Log() {
	}

	public static void d(String pTag, String pMessage) {
		log(Level.DEBUG, pTag, pMessage);
	}
	
	public static void d(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.DEBUG, pTag, pMessage, pThrowable);
	}
	
	public static void i(String pTag, String pMessage) {
		log(Level.INFO, pTag, pMessage);
	}
	
	public static void i(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.INFO, pTag, pMessage, pThrowable);
	}
	
	public static void w(String pTag, String pMessage) {
		log(Level.WARN, pTag, pMessage);
	}
	
	public static void w(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.WARN, pTag, pMessage, pThrowable);
	}
	
	public static void e(String pTag, String pMessage) {
		log(Level.ERROR, pTag, pMessage);
	}
	
	public static void e(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.ERROR, pTag, pMessage, pThrowable);
	}
	
	public static void log(Level pLevel, String pTag, String pMessage, Throwable pThrowable) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append(pMessage);
		messageBuilder.append('\n');
		messageBuilder.append(getStackTrace(pThrowable));
		log(pLevel, pTag, messageBuilder.toString());
	}
	
	public static void log(Level pLevel, String pTag, String pMessage) {
		String timeString = getTimestampString();
		String tagString = append(pTag, MAX_TAG_LENGTH, " ", true);
		if (pTag.length() > MAX_TAG_LENGTH) {
			tagString = pTag.substring(0, MAX_TAG_LENGTH - 3) + "...";
		}
		String transformedMessage = pMessage.replace("\n", "\n" + charString(' ', 53));
		String text = "[" + pLevel.mShort + "]" + SEPERATOR  + timeString + SEPERATOR
				+ tagString + SEPERATOR + transformedMessage;
		if (pLevel.equals(Level.ERROR) || pLevel.equals(Level.WARN)) {
			sSystemErr.println(text);
		} else {
			sSystemOut.println(text);
		}
		fileLog(text);
	}
	
	public static void setLogFolder(File pFolder) {
		if (sLogFolder == null) {
			sLogFolder = pFolder;
			Log.i(LOG_TAG, "Set log folder to '" + sLogFolder.getAbsolutePath() + "'");
			PrintStream newLog = initLogFile();
			Log.i(LOG_TAG, "Writing buffer to file...");
			sLogFileOut = newLog;
			writeBufferToLogFile();
		} else {
			Log.i(LOG_TAG, "Changing log folder from '" + sLogFolder + "' to '" + pFolder.getAbsolutePath() + "'");
			File oldLogFile = sLogFile;
			sLogFile = null;
			sLogFileOut.close();
			sLogFileOut = null;
			PrintStream newLogFileOut = initLogFile();
			Log.i(LOG_TAG, "Writing logs from old file to new file...");
			FileInputStream fis = null;
			byte[] oldData = null;
			try {
				fis = new FileInputStream(oldLogFile);
				oldData = new byte[fis.available()];
				fis.read(oldData);
				fis.close();
			} catch (IOException e) {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e1) {
						Log.w(LOG_TAG, "Failed to close InputStream");
					}
				}
				Log.w(LOG_TAG, "Failed to switch logfile! Continuing using old file");
				sLogFile = oldLogFile;
				try {
					sLogFileOut = new PrintStream(sLogFile);
				} catch (FileNotFoundException e1) {
					Log.w(LOG_TAG, "Old log file does not exist anymore!");
					Log.e(LOG_TAG, "NOT LOGGING TO FILE ANYMORE!");
				}
				return;
			}
			String oldLogs = new String(oldData, Charset.forName("UTF-8"));
			sLogFileOut = newLogFileOut;
			sLogFileOut.print(oldLogs);
		}
	}
	
	private static PrintStream initLogFile() {
		Log.i(LOG_TAG, "Initializing logfile");
		File newLogFile = new File(sLogFolder, getTimestampString().replace(':', '-') + ".log");
		int logNumber = -1;
		while (newLogFile.exists()) {
			logNumber++;
			Log.w(LOG_TAG, "Logfile for system start timestamp already exists! Using file with 'log." +
					logNumber + "' ending!");
			if (logNumber == 0) {
				newLogFile = new File(newLogFile.getAbsolutePath().replace(".log", ".log0"));
			} else {
				newLogFile = new File(newLogFile.getAbsolutePath().replace(".log" + (logNumber - 1), ".log" + logNumber));
			}
		}
		try {
			newLogFile.createNewFile();
		} catch (IOException e) {
			Log.w(LOG_TAG, "Failed to create logfile '" + newLogFile.getAbsolutePath() + "'!", e);
			return null;
		}
		PrintStream newLogFileOut;
		try {
			newLogFileOut = new PrintStream(newLogFile);
		} catch (FileNotFoundException e) {
			Log.e(LOG_TAG, "Internal error! Logfile does not exist after creating it! (" +
					newLogFile.getAbsolutePath() + ")");
			return null;
		}
		Log.i(LOG_TAG, "Logfile sucessfull initialized!");
		Log.i(LOG_TAG, "Logging now to file '" + newLogFile.getAbsolutePath() + "'");
		sLogFile = newLogFile;
		return newLogFileOut;
	}
	
	private static void writeBufferToLogFile() {
		sLogFileOut.print(sLogFileBuffer.toString());
		sLogFileBuffer.setLength(0);
	}

	private static void fileLog(String pText) {
		if (sLogFileOut != null) {
			sLogFileOut.println(pText);
		} else {
			sLogFileBuffer.append(pText);
			sLogFileBuffer.append('\n');
		}
	}

	private static String getStackTrace(Throwable pThrowable) {
		ByteArrayOutputStream out = null;
		try {
			out = new ByteArrayOutputStream();
			pThrowable.printStackTrace(new PrintStream(out));
			return new String(out.toByteArray(), Charset.forName("UTF-8"));
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// TODO add year to timestamp
	private static String getTimestampString() {
		Calendar calendar = Calendar.getInstance();
		// Calendar.JANUARY has value 0
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		int millisecond = calendar.get(Calendar.MILLISECOND);
		
		String monthString = appendZeros(month, 2);
		String dayString = appendZeros(day, 2);
		String hourString = appendZeros(hour, 2);
		String minuteString = appendZeros(minute, 2);
		String secondString = appendZeros(second, 2);
		String millisecondString = appendZeros(millisecond, 3);
		
		return monthString + "-" + dayString + " " + hourString + ":" + minuteString + ":" + secondString + "." + millisecondString;
	}
	
	private static String appendZeros(int pInteger, int pLength) {
		return append(Integer.toString(pInteger), pLength, "0", false);
	}
	
	private static String charString(char pChar, int pCount) {
		StringBuilder builder = new StringBuilder();
		for (int i=0; i<pCount; i++) {
			builder.append(pChar);
		}
		return builder.toString();
	}
	
	private static String append(String pString, int pLength, String pExtra, boolean pSuffix) {
		String string = pString;
		while (string.length() < pLength) {
			if (pSuffix) {
				string = string + pExtra;
			} else {
				string = pExtra + string;
			}
		}
		return string;
	}
	
	public static enum Level {
		
		DEBUG("D", 0),
		INFO("I", 1),
		WARN("W", 2),
		ERROR("E", 3);
		
		private String mShort;
		private int mPriority;
		
		private Level(String pShort, int pPriority) {
			mShort = pShort;
			mPriority = pPriority;
		}
	}
	
	private static class AutoLogOutputStream extends OutputStream {
		
		private final Level mLevel;
		private final String mLogTag;
		private final StringBuilder mMessageBuilder;
		
		public AutoLogOutputStream(Level pLevel, String pLogTag) {
			mLevel = pLevel;
			mLogTag = pLogTag;
			mMessageBuilder = new StringBuilder();
		}
		
		@Override
		public void write(int pChar) throws IOException {
			char character = (char) pChar;
			if (character == '\n') {
				Log.log(mLevel, mLogTag, mMessageBuilder.toString());
				mMessageBuilder.setLength(0);
			} else {
				mMessageBuilder.append(character);
			}
		}
	}
	
	private static class Properties {
		
		public static boolean exists;
		public static String logFolder;
		public static String logLevel;
		
		public static void load() {
			File properties = new File("log.properties");
			exists = properties.exists() && !properties.isDirectory();
			if (exists) {
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(properties), Charset.forName("UTF-8")));
					String line;
					while ((line = reader.readLine()) != null) {
						if (line.startsWith("logFolder = ")) {
							logFolder = line.substring(12);
						} else if (line.startsWith("logLevel = ")) {
							logLevel = line.substring(11);
						} else {
							Log.w(LOG_TAG, "Unknown line in properties file '" + line + "'");
						}
					}
				} catch (IOException e) {
					Log.w(LOG_TAG, "Exception while reading propertoes file", e);
				}
			} else {
				Log.w(LOG_TAG, "No properties file exists! Using defaults.");
				logLevel = Level.DEBUG.toString();
			}
		}
	}
}