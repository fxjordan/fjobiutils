package de.fjobilabs.util;

/**
 * Simple wrapper around an array, to easy use arrays
 * with generic types.
 * 
 * @param <T> Type of the elements stored in the wrapped array.
 * 
 * @author Felix Jordan
 * @since 11.09.2015 - 23:21:46
 * @version 1.0
 */
public class GenericArray<T> {
	
	/** The wrapped array. */
	private Object[] mElements;
	
	/**
	 * Creates a new {@code GenericArray} with a specified
	 * length and all elements set to {@code null}.
	 * 
	 * @param pLength Length of the array.
	 */
	public GenericArray(int pLength) {
		mElements = new Object[pLength];
	}
	
	/**
	 * Creates a new {@code GenericArray} from a Java array.<br>
	 * <br>
	 * The given Java array is used as the wrapped array by this
	 * class instead of copying the elements to a new array.
	 * 
	 * @param pElements The elements of the array.
	 */
	@SafeVarargs
	public GenericArray(T... pElements) {
		mElements = pElements;
	}
	
	/**
	 * Sets the element at a given position in the array.
	 * 
	 * @param pIndex The array position.
	 * @param pElement The element to set.
	 */
	public void set(int pIndex, T pElement) {
		mElements[pIndex] = pElement;
	}
	
	/**
	 * Returns the element at a given position in the array.
	 * 
	 * @param pIndex The array position.
	 * @return The element at the given position.
	 */
	@SuppressWarnings("unchecked")
	public T get(int pIndex) {
		return (T) mElements[pIndex];
	}
	
	/**
	 * Returns the length of the array.
	 * 
	 * @return The array length.
	 */
	public int length() {
		return mElements.length;
	}
}
