package de.fjobilabs.util;

/**
 * @author Felix Jordan
 * @since 20.12.2014 - 16:51:52
 * @version 1.0
 */
public class Vector2 {
	
	public static final float TO_RADIANS = (1 / 180.0f) * (float) Math.PI;
	public static final float TO_DEGREES = (1 / (float) Math.PI) * 180;
	
	public float x;
	public float y;
	
	public Vector2() {
	}
	
	public Vector2(float pX, float pY) {
		x = pX;
		y = pY;
	}
	
	public Vector2(Vector2 pOther) {
		x = pOther.x;
		y = pOther.y;
	}
	
	public Vector2 copy() {
		return new Vector2(x, y);
	}
	
	public Vector2 set(float pX, float pY) {
		x = pX;
		y = pY;
		return this;
	}
	
	public Vector2 set(Vector2 pOther) {
		x = pOther.x;
		y = pOther.y;
		return this;
	}
	
	public Vector2 add(float pX, float pY) {
		x += pX;
		y += pY;
		return this;
	}
	
	public Vector2 add(Vector2 pOther) {
		x += pOther.x;
		y += pOther.y;
		return this;
	}
	
	public Vector2 sub(float pX, float pY) {
		x -= pX;
		y -= pY;
		return this;
	}
	
	public Vector2 sub(Vector2 pOther) {
		x -= pOther.x;
		y -= pOther.y;
		return this;
	}
	
	public Vector2 mul(float pScalar) {
		x *= pScalar;
		y *= pScalar;
		return this;
	}
	
	public Vector2 nor() {
		float len = len();
		if (len != 0) {
			x = x / len;
			y = y / len;
		}
		return this;
	}
	
	public Vector2 rotate(float pAngle) {
		float rad = pAngle * TO_RADIANS;
		float sin = (float) Math.sin(rad);
		float cos = (float) Math.cos(rad);
		
		float newX = x * cos - y * sin;
		float newY = x * sin + y * cos;
		
		x = newX;
		y = newY;
		
		return this;
	}
	
	public float len() {
		return (float) Math.sqrt(x * x + y * y);
	}
	
	public float angle() {
		float angle = (float) (Math.atan2(y, x) * TO_DEGREES);
		if (angle < 0) {
			angle += 360;
		}
		return angle;
	}
	
	public float dist(float pX, float pY) {
		float distX = x - pX;
		float distY = y - pY;
		return (float) Math.sqrt(distX * distX + distY * distY);
	}
	
	public float dist(Vector2 pOther) {
		float distX = x - pOther.x;
		float distY = y - pOther.y;
		return (float) Math.sqrt(distX * distX + distY * distY);
	}
	
	public float distSquared(float pX, float pY) {
		float distX = x - pX;
		float distY = y - pY;
		return distX * distX + distY * distY;
	}
	
	public float distSquared(Vector2 pOther) {
		float distX = x - pOther.x;
		float distY = y - pOther.y;
		return distX * distX + distY * distY;
	}
}